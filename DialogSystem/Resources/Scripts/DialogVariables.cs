using UnityEngine;

using System.Collections;
using System.Collections.Generic;

using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System;

public class DialogVariables
{	
	[XmlRoot("GlobalVariables")]
	public class GlobalsContainer
	{
		[XmlElement("Global strNames")]
		public List<string> gblStrNames;
		
		[XmlElement("Global strValues")]
		public List<string> gblStrValues;
		
		[XmlElement("Global floatNames")]
		public List<string> gblFloatNames;
		
		[XmlElement("Global floatValues")]
		public List<float> 	gblFloatValues;
		
		[XmlElement("Global boolNames")]
		public List<string> gblBoolNames;
		
		[XmlElement("Global boolValues")]
		public List<bool> 	gblBoolValues;
	}

	public GlobalsContainer globals;

	private string pathFromAssets = "/Resources/GlobalsContainer.xml";
	private Dialog.DialogData dialog;
	
	public DialogVariables ( Dialog.DialogData data, GameObject player )
	{	
		dialog = data;
		ReadXML ( pathFromAssets );
	}
	
	/*--------------------------------------------------------------------
		READ XML
		
			Deserialize the passed XML file into a DialogData instance

	*///------------------------------------------------------------------
	public bool ReadXML(string filepath)
	{
		// LOAD FROM PATH
		if ( File.Exists(Application.dataPath + filepath) )
		{
			var stream = new FileStream( Application.dataPath + filepath, FileMode.Open);
			
			XmlSerializer serializer = new XmlSerializer( typeof(GlobalsContainer) );
			GlobalsContainer data = (GlobalsContainer)serializer.Deserialize( stream );
			
			stream.Close();
			globals = data;
			
			return true;
		}
		
		else 
		{
			var stream = new FileStream ( Application.dataPath + filepath, FileMode.CreateNew );
			
			globals = new GlobalsContainer();
			
			XmlSerializer serializer = new XmlSerializer(typeof(GlobalsContainer));
			serializer.Serialize(stream, globals);
			
			stream.Close();
			
			return false;
		}
	}
	
	/*--------------------------------------------------------------------
		SAVE XML
		
			Serializes the dialog into XML at the provided location

	*///------------------------------------------------------------------
	public void WriteXML(GlobalsContainer globals, string filepath)
	{
		//dialog.UpdateCollections();
		
		var stream = new FileStream( filepath, FileMode.Create);
		
		XmlSerializer serializer = new XmlSerializer(typeof(GlobalsContainer));
		serializer.Serialize(stream, globals);
		
		stream.Close();
	}
	
	public void Save ()
	{
		WriteXML ( globals, Application.dataPath + pathFromAssets );
	}
	
	public void changeNode( int newNodeID )
	{
		if ( newNodeID != -1 )
		{
			dialog.currentNode = dialog.nodeCollection[ newNodeID ];
		}
		
		// TERMINATE DIALOG SESSION
		else
		{
			Dialog.instance.terminateDialogSession();
		}
	}
	
	public void ProcessCondition ()
	{
		var varNode = dialog.currentNode.variable;
		
		string strVar = "";
		float floatVar = 0.0f;
		bool boolVar = false;
		
		if ( true )
		{			
			// { global , local }
			switch ( varNode.scope )
			{
			
			// global
			case 0:
				
				// { string , float , bool }
				switch ( varNode.variableType )
				{
					// string
				case 0:
					if ( varNode.writeIn )
						strVar = varNode.writeInVariable.ToString();
					
					else
						strVar = globals.gblStrValues[ varNode.variableTwo ];
					
					// { == , != }
					switch ( varNode.condition )
					{
						
					case 0:
						if ( globals.gblStrValues[ varNode.variableOne ].Equals( strVar ) )
						{
							changeNode ( varNode.nodeOut[0] );
						}
						else
						{
							changeNode ( varNode.nodeOut[1] );
						}
						break;
						
					case 1:
						if ( !globals.gblStrValues[ varNode.variableOne ].Equals( strVar ) )
						{
							changeNode ( varNode.nodeOut[0] );
						}
						else
						{
							changeNode ( varNode.nodeOut[1] );
						}
						break;
						
					default:
						break;
						
					}
					
					break;
					
				// float
				case 1:
					
					if ( varNode.writeIn )
						floatVar = float.Parse ( varNode.writeInVariable );
					
					else
						floatVar = globals.gblFloatValues[ varNode.variableTwo ];
					
					// { <, >, ==, != }
					switch ( varNode.condition )
					{
						
					case 0:
						if ( globals.gblFloatValues[ varNode.variableOne ].CompareTo( floatVar ) < 0 )
						{
							changeNode ( varNode.nodeOut[0] );
						}
						else
						{
							changeNode ( varNode.nodeOut[1] );
						}
						break;
						
					case 1:
						if ( globals.gblFloatValues[ varNode.variableOne ].CompareTo( floatVar ) > 0 )
						{
							changeNode ( varNode.nodeOut[0] );
						}
						else
						{
							changeNode ( varNode.nodeOut[1] );
						}
						break;
						
					case 2:
						if ( globals.gblFloatValues[ varNode.variableOne ].Equals( floatVar ) )
						{
							changeNode ( varNode.nodeOut[0] );
						}
						else
						{
							changeNode ( varNode.nodeOut[1] );
						}
						
						break;
						
					case 3:
						if ( !globals.gblFloatValues[ varNode.variableOne ].Equals( floatVar ) )
						{
							changeNode ( varNode.nodeOut[0] );
						}
						else
						{
							changeNode ( varNode.nodeOut[1] );
						}
						
						break;
						
					default:
						break;
					}
					
					break;
					
				// bool
				case 2:
					
					if ( varNode.writeIn )
						boolVar = bool.Parse ( varNode.writeInVariable );
					
					else
						boolVar = globals.gblBoolValues[ varNode.variableTwo ];
					
					switch ( varNode.condition )
					{
					case 0:
						if ( globals.gblBoolValues[ varNode.variableOne ].Equals( boolVar ) )
						{
							changeNode ( varNode.nodeOut[0] );
						}
						
						else
						{
							changeNode ( varNode.nodeOut[1] );
						}
						break;
						
					case 1:
						if ( !globals.gblBoolValues[ varNode.variableOne ].Equals( boolVar ) )
						{
							changeNode ( varNode.nodeOut[0] );
						}
						
						else
						{
							changeNode ( varNode.nodeOut[1] );
						}
						break;
						
					default:
						break;
					}
					break;
					
				default:
					break;
				}
				break;
				
			// local
			case 1:
				
				// { string , float , bool }
				switch ( varNode.variableType )
				{
				// string
				case 0:
					
					if ( varNode.writeIn )
						strVar = varNode.writeInVariable.ToString();
					
					else
						strVar = dialog.stringValues[ varNode.variableTwo ];
					
					// { == , != }
					switch ( varNode.condition )
					{						
					case 0:
						if ( dialog.stringValues[ varNode.variableOne ].Equals( strVar ) )
						{
							changeNode ( varNode.nodeOut[0] );
						}
						else
						{
							changeNode ( varNode.nodeOut[1] );
						}
						break;
						
					case 1:
						if ( !dialog.stringValues[ varNode.variableOne ].Equals( strVar ) )
						{
							changeNode ( varNode.nodeOut[0] );
						}
						else
						{
							changeNode ( varNode.nodeOut[1] );
						}
						break;
						
					default:
						break;	
					}
					break;
					
				// float
				case 1:
					
					if ( varNode.writeIn )
						floatVar = float.Parse( varNode.writeInVariable );
					
					else
						floatVar = dialog.floatValues[ varNode.variableTwo ];
					
					// { <, >, ==, != }
					switch ( varNode.condition )
					{
						
					case 0:
						if ( dialog.floatValues[ varNode.variableOne ].CompareTo( floatVar ) < 0 )
						{
							changeNode ( varNode.nodeOut[0] );
						}
						else
						{
							changeNode ( varNode.nodeOut[1] );
						}
						break;
						
					case 1:
						if ( dialog.floatValues[ varNode.variableOne ].CompareTo( floatVar ) > 0 )
						{
							changeNode ( varNode.nodeOut[0] );
						}
						else
						{
							changeNode ( varNode.nodeOut[1] );
						}
						break;
						
					case 2:
						if ( dialog.floatValues[ varNode.variableOne ].Equals( floatVar ) )
						{
							changeNode ( varNode.nodeOut[0] );
						}
						else
						{
							changeNode ( varNode.nodeOut[1] );
						}
						break;
						
					case 3:
						if ( !dialog.floatValues[ varNode.variableOne ].Equals( floatVar ) )
						{
							changeNode ( varNode.nodeOut[0] );
						}
						else
						{
							changeNode ( varNode.nodeOut[1] );
						}
						break;
						
					default:
						break;
						
					}
					
					break;
					
					// bool
				case 2:
					
					if ( varNode.writeIn )
						boolVar = bool.Parse ( varNode.writeInVariable );
					
					else
						boolVar = dialog.boolValues[ varNode.variableTwo ];
					
					switch ( varNode.condition )
					{
					case 0:
						if ( dialog.boolValues[ varNode.variableOne ].Equals( boolVar ) )
						{
							changeNode ( varNode.nodeOut[0] );
						}
						
						else
						{
							changeNode ( varNode.nodeOut[1] );
						}
						break;
						
					case 1:
						if ( !dialog.boolValues[ varNode.variableOne ].Equals( boolVar ) )
						{
							changeNode ( varNode.nodeOut[0] );
						}
						
						else
						{
							changeNode ( varNode.nodeOut[1] );
						}
						break;
						
					default:
						break;
					}
					break;
					
				default:
					break;
				}
				break;
				
			default:
				break;
			}
		}		
	}
	
	/*--------------------------------------------------------------------
		PROCESS VARIABLE NODE

	*///------------------------------------------------------------------
	public void ProcessVariable()
	{
		var varNode = dialog.currentNode.variable;
		
		string strVar = "";
		float floatVar = 0.0f;
		bool boolVar = false;
		
		if ( true )
		{			
			// { global , local }
			switch ( varNode.scope )
			{
				// global
			case 0:
				
				// { string , float , bool }
				switch ( varNode.variableType )
				{
					// string
				case 0:
					
					strVar = varNode.writeInVariable.ToString();
					
					// { set, append }
					switch ( varNode.condition )
					{
						// set
					case 0:
						Dialog.Global_stringValues[ varNode.variableOne ] = strVar;
						break;
						
						// append
					case 1:
						Dialog.Global_stringValues[ varNode.variableOne ] += strVar;
						break;
						
					default:
						break;
						
					}
					
					break;
					
					// float
				case 1:
					
					floatVar = float.Parse ( varNode.writeInVariable );
					
					// { add, subtract, multiply, divide }
					switch ( varNode.condition )
					{
						
					case 0:
						Dialog.Global_floatValues[ varNode.variableOne ] += floatVar;
						break;
						
					case 1:
						Dialog.Global_floatValues[ varNode.variableOne ] -= floatVar;
						break;
						
					case 2:
						Dialog.Global_floatValues[ varNode.variableOne ] *= floatVar;
						break;
						
					case 3:
						Dialog.Global_floatValues[ varNode.variableOne ] /= floatVar;
						
						break;
						
					default:
						break;
						
					}
					
					break;
					
					// bool
				case 2:
					
					// { set, invert }
					switch ( varNode.condition )
					{
					case 0:
						boolVar = bool.Parse ( varNode.writeInVariable );
						Dialog.Global_boolValues[ varNode.variableOne ] = boolVar;
						break;
						
					case 1:
						Dialog.Global_boolValues[ varNode.variableOne ] = 
							!Dialog.Global_boolValues[ varNode.variableOne ];
						break;
						
					default:
						break;
					}
					break;
					
				default:
					break;
				}
				break;
				
				// local
			case 1:
				
				// { string , float , bool }
				switch ( varNode.variableType )
				{
					// string
				case 0:
					
					strVar = varNode.writeInVariable.ToString();
					
					// { set, append }
					switch ( varNode.condition )
					{						
					case 0:
						dialog.stringValues[ varNode.variableOne ] = strVar;
						break;
						
					case 1:
						dialog.stringValues[ varNode.variableOne ] += strVar;
						break;
						
					default:
						break;	
					}
					break;
					
					// float
				case 1:
					
					floatVar = float.Parse( varNode.writeInVariable );
					
					// { add, subtract, multiply, divide }
					switch ( varNode.condition )
					{
						
						// add
					case 0:
						dialog.floatValues[ varNode.variableOne ] += floatVar;
						break;
						
					case 1:
						dialog.floatValues[ varNode.variableOne ] -= floatVar;
						break;
						
					case 2:
						dialog.floatValues[ varNode.variableOne ] *= floatVar;
						break;
						
					case 3:
						dialog.floatValues[ varNode.variableOne ] /= floatVar;
						break;
						
					default:
						break;
						
					}		
					break;
					
					// bool
				case 2:
					
					// { set, invert }
					switch ( varNode.condition )
					{
					case 0:
						boolVar = bool.Parse ( varNode.writeInVariable );
						dialog.boolValues[ varNode.variableOne ] = boolVar;
						break;
						
					case 1:
						dialog.boolValues[ varNode.variableOne ] = !dialog.boolValues[ varNode.variableOne ];
						break;
						
					default:
						break;
					}
					break;
					
				default:
					break;
				}
				break;
				
			default:
				break;
			}
		}
	}
	
} // END OF CLASS
