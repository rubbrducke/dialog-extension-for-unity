﻿using UnityEngine;
using System.Collections;

public class DialogController : MonoBehaviour {

	public GUISkin guiSkin;
	public Dialog activeDialog;
	
	private CharacterMovement playerController;
	private MouseLook playerMouselook;
	private GameObject target;
	
	public float maxEngageDistance;
	public Vector2 screenDivisor;
	
	public enum GUIShape { List, Circular }
	public GUIShape shape;
	public Vector2 choiceboxSize;
	
	public int arc;
	public int degreeOffset;
	public Vector2 spaceOffset;
	public bool counterclock;
	
	public Vector2 buffer;
	public int columns;
	
	void Start () 
	{
		playerController = gameObject.GetComponent<CharacterMovement>();
		playerMouselook = gameObject.GetComponent<MouseLook>();
	}
	
	/*--------------------------------------------------------------------
		ENGAGE NEW DIALOG
		
			if ( activeDialog ): 
				If an activeDialog is available then
				check if they're asking for a new node
			
			else if ( Input ):
				If there is no activeDialog but there is input then
				do a raycast to determine if there's an object to talk to
				
			else
				There is no activeDialog and nobody to engage with,
				so we set inDialog to false
			
	*///------------------------------------------------------------------
	void Update () 
	{
		if ( activeDialog )
		{			
			if ( Input.GetButtonDown("Attack1") 
				&& activeDialog.dialog.currentNode.type != Dialog.DialogNodeTypes.typeChoice )
			{
				activeDialog.NextNode ();
			}
			
			// nodes to skip through
			else if ( activeDialog.dialog.currentNode.type == Dialog.DialogNodeTypes.typeStart
			    || activeDialog.dialog.currentNode.type == Dialog.DialogNodeTypes.typeCondition
			    || activeDialog.dialog.currentNode.type == Dialog.DialogNodeTypes.typeVariable )
			{
				activeDialog.NextNode();
			}
		
		}
		
		else if ( Input.GetButtonDown("Attack1") )
		{
			RaycastHit hit;
			
			var rayPos = transform.position;
			rayPos.y = transform.GetComponent<Collider>().bounds.center.y;
			
			if ( Physics.Raycast ( rayPos, transform.TransformDirection( Vector3.forward )*10, out hit ) )
			{	
				target = hit.collider.gameObject;
				activeDialog = target.GetComponent<Dialog>();
			}
		}
	
	}
	
	/*--------------------------------------------------------------------
		ON GUI

			If there's an activeDialog it displays the GUI for that
			dialog

	*///------------------------------------------------------------------
	void OnGUI()
	{		
		if ( activeDialog )
		{
			if ( activeDialog.dialog.currentNode.type == Dialog.DialogNodeTypes.typeChoice )
			{
				speechGUI();
				
				if ( shape == GUIShape.Circular )
					radialDisplay();
				
				if ( shape == GUIShape.List )
					listDisplay();
			}
			
			else if ( activeDialog.dialog.currentNode.type == Dialog.DialogNodeTypes.typeText )
			{
				speechGUI();
			}
			
		}
		
	} // OnGUI()
	
	
	/*--------------------------------------------------------------------
		SPEECH GUI
		
			GUI for the speech

	*///------------------------------------------------------------------
	public void speechGUI()
	{
		Rect displayRect = new Rect(10, 10, 400, 60);
		float screenY = Screen.height/6;
		displayRect.center = new Vector2( Screen.width/2, screenY + 60 );
		
		var centeredStyle = GUI.skin.GetStyle("Box");
		centeredStyle.alignment = TextAnchor.MiddleCenter;
		
		GUI.skin.box.wordWrap = true;
		GUI.skin.box.alignment = TextAnchor.MiddleCenter;
		
		GUI.Box( displayRect, activeDialog.dialog.currentNode.text, centeredStyle );
		
		displayRect = new Rect( 10, 10, 200, 25 );
		displayRect.center = new Vector2( Screen.width/2, screenY );
		GUI.Box ( displayRect, "speaker", centeredStyle );
	}
	
	/*--------------------------------------------------------------------
		LIST DISPLAY
		
			List-style GUI for the dialog choices

	*///------------------------------------------------------------------
	public void listDisplay()
	{
		int answersCount = activeDialog.dialog.currentNode.answers.Count;
		
		int itemsPerColumn = answersCount / columns;
		int itemNum = 0;
		
		float screenX = Screen.width / screenDivisor.x;
		float screenY = Screen.height / screenDivisor.y;
		
		if ( columns > 1 )
			screenX = screenX - (choiceboxSize.x/2 + buffer.x/2)*(columns-1);
			
		float guiX;
		float guiY;
		
		float listHeight = 0;
		
		foreach ( Dialog.AnswerNode answer in activeDialog.dialog.currentNode.answers )
		{
			if ( itemNum == itemsPerColumn )
			{
				screenX = screenX + choiceboxSize.x + buffer.x;
				listHeight = 0;
				itemNum = 0;
			}
			
			guiX = screenX;
			guiY = screenY;
			
			Rect displayR = new Rect(0, 0, choiceboxSize.x, choiceboxSize.y);
			displayR.center = new Vector2(guiX, guiY + listHeight);
			
			GUI.skin.button.wordWrap = true;
			
			if ( GUI.Button( displayR, answer.text ) )
			{
				activeDialog.NextNodeByID ( answer.nodeOut );
			}
			listHeight = listHeight + choiceboxSize.y + buffer.y;
			
			itemNum++;
		}
	}
	
	/*--------------------------------------------------------------------
		RADIAL DISPLAY

			Radial-style GUI for the dialog choices

	*///------------------------------------------------------------------
	public void radialDisplay()
	{		
		int counterclockwise = ( counterclock ? 1 : -1 );
		arc = Mathf.Clamp (arc, 0, 360);
		
		int answersLength = activeDialog.dialog.currentNode.answers.Count;
		int answersCount = answersLength;
		
		if ( arc != 360 )
		{
			answersCount = answersLength - 1;
		}
		
		float radialTic = (arc / answersCount) * Mathf.Deg2Rad;
		float radialOffset = degreeOffset * Mathf.Deg2Rad;
		
		float screenX = Screen.width / screenDivisor.x;
		float screenY = Screen.height / screenDivisor.y;
		
		int counter = 0;
		
		foreach ( Dialog.AnswerNode answer in activeDialog.dialog.currentNode.answers )
		{
			float rad = counterclockwise*((counter*radialTic)+radialOffset);
			
			float guiX = screenX + Mathf.Cos (rad)*spaceOffset.x;
			float guiY = screenY + Mathf.Sin (rad)*spaceOffset.y;
			
			Rect displayR = new Rect(0, 0, 200, 40);
			displayR.center = new Vector2(guiX, guiY);
			
			GUI.skin.button.wordWrap = true;
			
			if ( GUI.Button( displayR, answer.text ) )
			{
				activeDialog.NextNodeByID ( answer.nodeOut );
			}
			counter++;
		}
	}
}
