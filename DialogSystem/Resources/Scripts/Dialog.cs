﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System;

public class Dialog : MonoBehaviour {
	
	/*####################################################################
	
		D A T A   C L A S S E S
		
			Classes that store dialog information and are
			tagged for XML serialization and deserialization
			
			Following classes:
				public class DialogData
				public class DialogNode
				public class AnswerNode
				public enum DialogNodeTypes
		
	*///##################################################################
	
	/*####################################################################
		DIALOG DATA
		
			Upper-level data class. Contains the character's name,
			the ID of the start node, the current node, and the
			collection of nodes.

	*///##################################################################
	[XmlRoot("dialogData")]
	public class DialogData
	{		
		[XmlElement("CharacterName")]
		public string characterName;
		
		[XmlElement("Speakers")]
		public List<string> speakers = new List<string>();
		
		[XmlElement("dialogNode")]
		public List<DialogNode> nodes = new List<DialogNode>();
		
		[XmlElement("floatNames")]
		public List<string> floatNames = new List<string>();
		
		[XmlElement("floatValues")]
		public List<float> floatValues = new List<float>();
		
		[XmlElement("stringNames")]
		public List<string> stringNames = new List<string>();
		
		[XmlElement("stringValues")]
		public List<string> stringValues = new List<string>();
		
		[XmlElement("boolNames")]
		public List<string> boolNames = new List<string>();
		
		[XmlElement("boolValues")]
		public List<bool> boolValues = new List<bool>();
		
		[XmlIgnore]
		public int startNode;
		
		[XmlIgnore]
		private int newNodeID = 1;
		
		[XmlIgnore]
		public DialogNode currentNode;
		
		[XmlIgnore]
		public Dictionary<int, DialogNode> nodeCollection = new Dictionary<int, DialogNode>();
		
		public DialogData()
		{
		}
		
		public DialogNode createNewNode( DialogNodeTypes nodeType )
		{			
			while ( !checkID() )
			{
				continue;
			}

			DialogNode newNode = new DialogNode( newNodeID, nodeType, 0 );
			
			nodes.Add ( newNode );
			nodeCollection.Add( newNode.id, newNode );
			
			newNodeID++;
			
			return newNode;
		}
		
		public bool checkID()
		{
			if ( nodeCollection.ContainsKey ( newNodeID ) )
			{
				newNodeID++;
				
				return false; // ID currently in use.
			}
			else
			{
				return true; // ID not in use, so it can be used now.
			}
		}
	}
	
	/*####################################################################
		DIALOG NODE
		
			Data class to hold node information, and any
			sub-nodes (such as AnswerNode)
	
	*///##################################################################
	public class DialogNode
	{
		[XmlAttribute]
		public DialogNodeTypes type;
		
		[XmlAttribute]
		public int id;
		
		[XmlAttribute]
		public int nodeOut;
		
		[XmlAttribute]
		public float x;
		
		[XmlAttribute]
		public float y;
		
		[XmlElement("Speaker")]
		public int speaker;
		
		[XmlElement("answerNode")]
		public List<AnswerNode> answers = new List<AnswerNode>();
		
		[XmlElement("variableNode")]
		public VariableNode variable;
		
		[XmlText]
		public string text;
		
		public DialogNode()
		{
		}
		
		public DialogNode(int newID, Dialog.DialogNodeTypes nodeType, int defaultSpeaker)
		{
			id = newID;
			type = nodeType;
			speaker = defaultSpeaker;
			nodeOut = -1;
			text = "";
			x = 0;
			y = 0;
			
			if ( type == DialogNodeTypes.typeChoice )
			{
				answers.Add ( new AnswerNode() );
				answers.Add ( new AnswerNode() );
			}
			
			else if ( type == DialogNodeTypes.typeCondition )
			{
				variable = new VariableNode();
				variable.coordinates.Add ( new Vector2(0, 0) );
				variable.coordinates.Add ( new Vector2(0, 0) );
				
				variable.nodeOut.Add ( -1 );
				variable.nodeOut.Add ( -1 );
			}
			
			else if ( type == DialogNodeTypes.typeVariable )
			{
				variable = new VariableNode();
			}
		}
	}
	
	/*####################################################################
		ANSWER NODE
		
			Sub-node of a Choice node.
		
	*///##################################################################
	public class AnswerNode
	{		
		[XmlAttribute]
		public DialogNodeTypes type;
		
		[XmlAttribute]
		public int nodeOut = -1;
		
		[XmlAttribute]
		public float x = 0;
		
		[XmlAttribute]
		public float y = 0;
		
		[XmlText]
		public string text;
	}
	
	/*####################################################################
		VARIABLE NODE
		
			Sub-node of a Choice node.
		
	*///##################################################################
	public class VariableNode
	{		
		[XmlAttribute]
		public DialogNodeTypes type;
		
		[XmlAttribute]
		public int scope;
		
		[XmlAttribute]
		public int variableType;
		
		[XmlAttribute]
		public bool writeIn = false;
		
		[XmlAttribute]
		public bool invertBool = false;
		
		[XmlElement("nodesOut")]
		public List<int> nodeOut = new List<int>();
		
		[XmlElement("coordinates")]
		public List<Vector2> coordinates = new List<Vector2>();
		
		[XmlElement("Condition")]
		public int condition;
		
		[XmlElement("VariableOne")]
		public int variableOne;
		
		[XmlElement("VariableTwo")]
		public int variableTwo;
		
		[XmlElement("WriteIn Variable")]
		public string writeInVariable;
		
		[XmlText]
		public string text;
	}
	
	/*####################################################################
		DIALOG NODE TYPES
		
			Enums used to identify the type of a given node.
			Type determines behavior and appearance.
			
	*///##################################################################
	public enum DialogNodeTypes
	{
		[XmlEnum("text")]
		typeText,
		
		[XmlEnum("choice")]
		typeChoice,
		
		[XmlEnum("answer")]
		typeAnswer,
		
		[XmlEnum("condition")]
		typeCondition,
		
		[XmlEnum("variable")]
		typeVariable,
		
		[XmlEnum("start")]
		typeStart
	}
		
	/*####################################################################
	
		D I A L O G   C L A S S
		
			Everything from here belongs specifically to this
			Dialog class, and not a nested class
			
			Index:
				void Start
				public DialogData ReadXML
				public void WriteXML
				public void LoadDialog
				public void NextNode

	*///##################################################################
	
	public static List<string> Global_floatNames = new List<string>();
	public static List<float> Global_floatValues = new List<float>();
	
	public static List<string> Global_stringNames = new List<string>();
	public static List<string> Global_stringValues = new List<string>();
	
	public static List<string> Global_boolNames = new List<string>();
	public static List<bool> Global_boolValues = new List<bool>();
	
	public static Dictionary<string, float> Global_floatVariables = new Dictionary<string, float>();
	public static Dictionary<string, string> Global_stringVariables = new Dictionary<string, string>();
	public static Dictionary<string, bool> Global_boolVariables = new Dictionary<string, bool>();
	
	public static DialogNode start;
	
	public string xmlFilename = "";
	public DialogData dialog;
	public DialogVariables variables;
	public static Dialog instance;
	
	private GameObject player;
	
	/*####################################################################
		START
		
			MonoBehavior that runs when object is initialized

	*///##################################################################
	void Start()
	{		
		LoadDialog();
		player = GameObject.FindGameObjectWithTag("Player");
		instance = this;
	}
	
	/*####################################################################
		READ XML
		
			Deserialize the passed XML file into a DialogData instance

	*///##################################################################
	public bool ReadXML(string filename)
	{
		/*
		if ( File.Exists(Application.dataPath + filepath) )
		{			
			var stream = new FileStream( Application.dataPath + filepath, FileMode.Open);
		
			XmlSerializer serializer = new XmlSerializer( typeof(DialogData) );
			DialogData data = (DialogData)serializer.Deserialize( stream );
			
			stream.Close();
			dialog = data;
			
			return true;
		}
		*/
		
		TextAsset textFile = Resources.Load( filename, typeof(TextAsset) ) as TextAsset;
		
		if ( textFile != null )
		{
			var serializer = new XmlSerializer( typeof(DialogData) );
			
			var reader = new System.IO.StringReader( textFile.text );
			DialogData data = (DialogData) serializer.Deserialize( reader );
			
			reader.Close ();
			dialog = data;
			
			return true;
		}
		
		else 
		{			
			var stream = new FileStream ( Application.dataPath + "/Resources/" + filename + ".xml", FileMode.CreateNew );

			dialog = new DialogData();
			start = new DialogNode( dialog.nodes.Count, Dialog.DialogNodeTypes.typeStart, 0);
			
			dialog.nodes.Add (start);
			
			XmlSerializer serializer = new XmlSerializer(typeof(DialogData));
			serializer.Serialize(stream, dialog);
			
			stream.Close();
			
			return false;
		}
	}
	
	/*####################################################################
		SAVE XML
		
			Serializes the dialog into XML at the provided location

	*///##################################################################
	public void WriteXML( DialogData dialog, string filename )
	{
		var stream = new FileStream( Application.dataPath + "/Resources/" + filename + ".xml", FileMode.Create);
		
		XmlSerializer serializer = new XmlSerializer(typeof(DialogData));
		serializer.Serialize(stream, dialog);
		
		stream.Close();
	}

	/*####################################################################
		LOAD DIALOG
		
			Reads the XML, fills the nodeCollection, and assigns the currentNode as
			the start node

	*///##################################################################
	public bool LoadDialog()
	{
		player = GameObject.FindGameObjectWithTag("Player");
		
		if ( xmlFilename != "" )
		{
			
			ReadXML ( xmlFilename );

			foreach ( DialogNode node in dialog.nodes )
			{
				dialog.nodeCollection.Add (node.id, node);
			}
			
			if ( dialog.speakers.Count == 0 || dialog.speakers == null )
			{
                if( player != null )
                {
                    dialog.speakers.Add(player.name);
                }
				dialog.speakers.Add(dialog.characterName);
			}
			
			dialog.currentNode = dialog.nodeCollection[dialog.startNode];
			variables = new DialogVariables ( dialog, player );
			
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/*####################################################################
		NEXT NODE

			Sets the new currentNode to the nodeOut of the currentNode
			and executes some behavior depending on the node type

	*///##################################################################
	public void NextNode()
	{
		Time.timeScale = 0;
		
		// check this
		if ( dialog.currentNode.type == DialogNodeTypes.typeVariable )
		{
			variables.ProcessVariable();
		}
		
		// AND do this
		if ( dialog.currentNode.type == DialogNodeTypes.typeCondition )
		{
			variables.ProcessCondition ();
		}
		
		else if ( dialog.currentNode.nodeOut != -1 )
		{
			dialog.currentNode = dialog.nodeCollection[ dialog.currentNode.nodeOut ];
		}

		else
		{
			terminateDialogSession();
		}
	}
	
	public void terminateDialogSession()
	{
		Time.timeScale = 1;
		// update file with current variable values (global and local)
		WriteXML (dialog, xmlFilename);
		variables.Save ();
		
		// terminate this dialog session
		dialog.currentNode = dialog.nodeCollection[ dialog.startNode ];
		player.GetComponent<DialogController>().activeDialog = null;
	}
	
	/*####################################################################
		NEXT NODE BY ID (int id)

			Sets the new currentNode to the nodeOut of the currentNode
			and executes some behavior depending on the node type

	*///##################################################################
	public void NextNodeByID(int id)
	{		
		if ( id != -1 )
		{
			dialog.currentNode = dialog.nodeCollection[ id ];
		}
		else
		{
			terminateDialogSession();
		}
	}
	
} // Dialog class