﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public abstract class NodeWindow
{
	public Rect position;
	public string windowName = "";
	public Color defaultColor = GUI.color;
	
	public Dialog.DialogNode node;
	public List<Rect> nodesIn = new List<Rect>();
	
	public int speakerIndex = 0;
	public string[] speaker = { "Other", "Player" };
	
	public NodeWindow() { }
	
	/*--------------------------------------------------------------------
			NODE WINDOW ( DialogNode )
		
				Constructor for a basic NodeWindow

	*///------------------------------------------------------------------
	public NodeWindow( Dialog.DialogNode newNode )
	{
		node = newNode;
		position = new Rect( node.x, node.y, 150, 150 );
	}
	
	/*--------------------------------------------------------------------
			CREATE NODE WINDOW
		
				Abstract method used to define the appearance of a child NodeWindow

	*///------------------------------------------------------------------
	public abstract void CreateNodeWindow(int id);
	
	
	/*--------------------------------------------------------------------
			SHOW
		
				Returns a GUI Window for this NodeWindow

	*///------------------------------------------------------------------
	public Rect Show()
	{
		return GUILayout.Window (node.id, new Rect(position.x, position.y, position.width, position.height), 
		                   CreateNodeWindow, windowName);
	}
	
	
	/*--------------------------------------------------------------------
			NODE HEAD
		
				Standard method to define the header appearance of every NodeWindow

	*///------------------------------------------------------------------
	public void NodeHead()
	{			
		// BUTTON: REMOVE NODE
		GUI.color = Color.red;
		if( GUI.Button( new Rect( position.width-12.5f, 2.5f, 10, 10), "") )
		{
			DialogWindow.Instance.RemoveNode( this );
		}
		
		// BUTTON: SELECT AS NEXT NODE
		GUI.color = Color.grey;
		if( GUI.Button( new Rect( 2.5f, 2.5f, 10, 10), "") )
		{
			DialogWindow.Instance.SelectedNext( this.node.id );
		}
		
		// reset GUI Color
		GUI.color = defaultColor;
	}
}

/*--------------------------------------------------------------------
	START WINDOW
		
		MonoBehavior that runs when object is initialized

*///------------------------------------------------------------------
public class StartWindow : NodeWindow
{		
	public StartWindow( Dialog.DialogNode newNode )
	{
		node = newNode;
		windowName = "ID: " + newNode.id;
		node.type = Dialog.DialogNodeTypes.typeStart;
		position = new Rect( node.x, node.y, 50, 75 );
	}
	
	public override void CreateNodeWindow(int id)
	{
		GUI.color = Color.blue;
		if( GUI.Button( new Rect( position.width-12.5f, 2.5f, 10, 10), "") )
		{
			DialogWindow.Instance.SelectedInitial( this.node.id );
		}
		GUI.color = defaultColor;
		
		EditorGUILayout.LabelField ( "String Variable Count: ",
		                            "" + DialogWindow.Instance.dialogInst.dialog.stringNames.Count );
		
		EditorGUILayout.LabelField ( "Float Variable Count: ",
		                            "" + DialogWindow.Instance.dialogInst.dialog.floatNames.Count );
		
		EditorGUILayout.LabelField ( "Boolean Variable Count: ",
		                            "" + DialogWindow.Instance.dialogInst.dialog.boolNames.Count );
		
		node.x = position.x;
		node.y = position.y;
		GUI.DragWindow();
	}
}

/*--------------------------------------------------------------------
	TEXT WINDOW
		
		MonoBehavior that runs when object is initialized

*///------------------------------------------------------------------
public class TextWindow : NodeWindow
{		
	public TextWindow( Dialog.DialogNode newNode )
	{
		node = newNode;
		windowName = "ID: " + newNode.id;
		node.type = Dialog.DialogNodeTypes.typeText;
		position = new Rect( node.x, node.y, 200, 120 );
	}
	
	public override void CreateNodeWindow(int id)
	{
		NodeHead();

		GUILayout.BeginVertical();
		
		// POPUP: SPEAKER
		speakerIndex = EditorGUILayout.Popup( speakerIndex, speaker );
		node.speaker = speakerIndex;
		
		GUILayout.BeginHorizontal();
		
		// BOX: DESCRIBES TEXT FIELD
		GUILayout.Box( "Dialog:", GUILayout.ExpandWidth(true) );

		// BUTTON: SELECT INITIAL NODE
		GUI.color = Color.blue;
		if ( GUILayout.Button( "", GUILayout.MaxWidth( 15.0f ) ) )
		{
			DialogWindow.Instance.SelectedInitial( this.node.id );
		}
		GUI.color = defaultColor;
		GUILayout.EndHorizontal();
		
		// TEXT AREA: DIALOG
		node.text = EditorGUILayout.TextArea( node.text, GUILayout.MaxHeight( position.height - 80 ) );
		
		GUILayout.EndVertical();
		
		node.x = position.x;
		node.y = position.y;
		GUI.DragWindow();
	}
}

/*--------------------------------------------------------------------
	CHOICE WINDOW

*///------------------------------------------------------------------
public class ChoiceWindow : NodeWindow
{
	private static int textAreaHeight = 70;
	
	public ChoiceWindow( Dialog.DialogNode newNode )
	{
		node = newNode;
		windowName = "ID: " + newNode.id;
		node.type = Dialog.DialogNodeTypes.typeChoice;
		position = new Rect( node.x, node.y, 200, 300);
	}
	
	public override void CreateNodeWindow(int id)
	{		
		position.height = textAreaHeight*(node.answers.Count+1) + 90;
		
		NodeHead();
		
		GUILayout.BeginVertical();
		
			// POPUP: SPEAKER
			speakerIndex = EditorGUILayout.Popup( speakerIndex, speaker );
			node.speaker = speakerIndex;
			
			// BOX: DIALOG TITLE
			GUILayout.Box( "Dialog:", GUILayout.ExpandWidth(true) );
			
			// TEXT AREA: QUESTION/SPEECH
			EditorStyles.textField.wordWrap = true;
			node.text = EditorGUILayout.TextArea( node.text, GUILayout.MaxHeight(textAreaHeight) );
			
			int indexCounter = 0;
			foreach ( Dialog.AnswerNode answer in node.answers )
			{
				GUILayout.BeginHorizontal();
				
					// BUTTON: REMOVE ANSWER
					if ( node.answers.Count > 1 )
					{
						GUI.color = Color.red;
						if ( GUILayout.Button( "", GUILayout.MaxWidth(15.0f) ) )
						{
							DialogWindow.Instance.SelectedAnswerToRemove( this.node.id, answer );
						}
						GUI.color = defaultColor;
					}
					
					// BOX: CHOICE
					GUILayout.Box ( "Choice", GUILayout.ExpandWidth(true) );
					
					// BUTTON: SELECT ANSWER
					GUI.color = Color.blue;
					if ( GUILayout.Button( "", GUILayout.MaxWidth(15.0f) ) )
					{
						DialogWindow.Instance.SelectedAnswer( this.node.id, indexCounter );
					}
					GUI.color = defaultColor;
					
					if (Event.current.type == EventType.Repaint) 
					{
						var answerRect = GUILayoutUtility.GetLastRect();
						answer.x = node.x + answerRect.x;
						answer.y = node.y + answerRect.y;
					}
					
				GUILayout.EndHorizontal();
				
				answer.text = EditorGUILayout.TextArea( answer.text, GUILayout.MaxHeight(textAreaHeight) );
				indexCounter++;
			}
				
			// BUTTON: ADD NODE
			if( GUILayout.Button( "Add Choice") )
			{
				node.answers.Add( new Dialog.AnswerNode() );
			}
			
		GUILayout.EndVertical();
		
		position.height = textAreaHeight*(node.answers.Count+1) + 90;
		
		node.x = position.x;
		node.y = position.y;
		GUI.DragWindow();
	}
}

/*--------------------------------------------------------------------
	CONDITION WINDOW
		
		MonoBehavior that runs when object is initialized

*///------------------------------------------------------------------
public class ConditionWindow : NodeWindow
{		
	public string[] globalLocal = { "Global", "Local" };
	public string[] varType = { "String", "Float", "Bool" };
	
	public string[] strBoolOperation = { "==", "!=" };
	public string[] floatOperation = { "<", ">", "==", "!=" };
	
	public ConditionWindow( Dialog.DialogNode newNode )
	{
		node = newNode;
		windowName = "ID: " + newNode.id;
		node.type = Dialog.DialogNodeTypes.typeCondition;
		position = new Rect( node.x, node.y, 250, 150 );
	}
	
	public override void CreateNodeWindow(int id)
	{
		NodeHead();		
		
		GUILayout.BeginVertical();
			
			// GLOBAL/LOCAL TOOLBAR
			node.variable.scope = GUILayout.Toolbar ( node.variable.scope, globalLocal );
			
			// VARIABLE TYPE TOOLBAR
			node.variable.variableType = GUILayout.Toolbar ( node.variable.variableType, varType );
			
			GUILayout.Space( 10.0f );
			
			GUILayout.BeginHorizontal();
			
				// BOX: IF
				GUILayout.Box ( "If:", GUILayout.ExpandWidth(true) );
				
				// BUTTON: SELECT INITIAL
				GUI.color = Color.blue;
				if ( GUILayout.Button( "", GUILayout.MaxWidth(15.0f) ) )
				{
					DialogWindow.Instance.SelectedAnswer( this.node.id, 0 );
				}
				GUI.color = defaultColor;
						
				if (Event.current.type == EventType.Repaint) 
				{
					var conditionRect = GUILayoutUtility.GetLastRect();
					Vector2 temp = new Vector2( node.x + conditionRect.x, node.y + conditionRect.y );
					node.variable.coordinates[0] = temp;
				}
			
			GUILayout.EndHorizontal();
			
			string[] variableNames = new string[0];
			if ( node.variable.scope == 0 )
			{	
				// string
				if ( node.variable.variableType == 0 )
				{
					variableNames = new string[ DialogWindow.Instance.dialogInst.variables.globals.gblStrNames.Count ];
					
					int index = 0;
					foreach ( string name in DialogWindow.Instance.dialogInst.variables.globals.gblStrNames )
					{
						variableNames[ index ] = name;
						index++;
					}	
				}
				// float
				else if ( node.variable.variableType == 1 )
				{
					variableNames = new string[ DialogWindow.Instance.dialogInst.variables.globals.gblFloatNames.Count ];
					
					int index = 0;
					foreach ( string name in DialogWindow.Instance.dialogInst.variables.globals.gblFloatNames )
					{
						variableNames[ index ] = name;
						index++;
					}
				}
				// bool
				else if ( node.variable.variableType == 2 )
				{
					variableNames = new string[ DialogWindow.Instance.dialogInst.variables.globals.gblBoolNames.Count ];
					
					int index = 0;
					foreach ( string name in DialogWindow.Instance.dialogInst.variables.globals.gblBoolNames )
					{
						variableNames[ index ] = name;
						index++;
					}
				}
			}
			
			else
			{				
				// string
				if ( node.variable.variableType == 0 )
				{
					variableNames = new string[ DialogWindow.Instance.dialogInst.dialog.stringNames.Count ];
					
					int index = 0;
					foreach ( string name in DialogWindow.Instance.dialogInst.dialog.stringNames )
					{
						variableNames[ index ] = name;
						index++;
					}
				}
				// float
				else if ( node.variable.variableType == 1 )
				{
					variableNames = new string[ DialogWindow.Instance.dialogInst.dialog.floatNames.Count ];
					
					int index = 0;
					foreach ( string name in DialogWindow.Instance.dialogInst.dialog.floatNames )
					{
						variableNames[ index ] = name;
						index++;
					}
				}
				// bool
				else if ( node.variable.variableType == 2 )
				{
					variableNames = new string[ DialogWindow.Instance.dialogInst.dialog.boolNames.Count ];
					
					int index = 0;
					foreach ( string name in DialogWindow.Instance.dialogInst.dialog.boolNames )
					{
						variableNames[ index ] = name;
						index++;
					}
				}
			}
			
			// string
			if ( node.variable.variableType == 0 )
			{
				GUILayout.BeginHorizontal();
				
					node.variable.variableOne = EditorGUILayout.Popup( node.variable.variableOne, variableNames );
					node.variable.condition = GUILayout.Toolbar ( node.variable.condition, strBoolOperation );
					
				GUILayout.EndHorizontal();
			}
			// float
			else if ( node.variable.variableType == 1 )
			{
				GUILayout.BeginHorizontal();
				
					node.variable.variableOne = EditorGUILayout.Popup( node.variable.variableOne, variableNames );
					node.variable.condition = GUILayout.Toolbar ( node.variable.condition, floatOperation );
					
				GUILayout.EndHorizontal();
			}
			// bool
			else if ( node.variable.variableType == 2 )
			{
				GUILayout.BeginHorizontal();
				
					node.variable.variableOne = EditorGUILayout.Popup( node.variable.variableOne, variableNames );
					node.variable.condition = GUILayout.Toolbar ( node.variable.condition, strBoolOperation );
					
				GUILayout.EndHorizontal();
			}
			
			GUILayout.BeginHorizontal();
			
				// TOGGLE: WRITE-IN
				node.variable.writeIn = GUILayout.Toggle ( node.variable.writeIn, "Write In" );
				
				// TEXT FIELD: COMPARISON VALUE
				if ( node.variable.writeIn )
					node.variable.writeInVariable = EditorGUILayout.TextField( node.variable.writeInVariable );
		
				// POPUP: COMPARISION VALUE FROM LIST
				else
				{
					node.variable.variableTwo = EditorGUILayout.Popup( node.variable.variableTwo, variableNames );
				}	
			
			GUILayout.EndHorizontal();
			
			GUILayout.Space( 10.0f );
			
			GUILayout.BeginHorizontal();
			
				// BOX: ELSE
				GUILayout.Box ( "Else:", GUILayout.ExpandWidth(true) );
				
				// BUTTON: SELECT INITIAL
				GUI.color = Color.blue;
				if ( GUILayout.Button( "", GUILayout.MaxWidth(15.0f) ) )
				{
					DialogWindow.Instance.SelectedAnswer( this.node.id, 1 );
				}
				GUI.color = defaultColor;
				
				if (Event.current.type == EventType.Repaint) 
				{
					var conditionRect = GUILayoutUtility.GetLastRect();
					Vector2 temp = new Vector2( node.x + conditionRect.x, node.y + conditionRect.y );
					node.variable.coordinates[1] = temp;
				}
				
			GUILayout.EndHorizontal();
		
		GUILayout.EndVertical();
				
		node.x = position.x;
		node.y = position.y;
		GUI.DragWindow();
	}
}

/*--------------------------------------------------------------------
	VARIABLE WINDOW
		
		MonoBehavior that runs when object is initialized

*///------------------------------------------------------------------
public class VariableWindow : NodeWindow
{		
	public string[] globalLocal = { "Global", "Local" };
	public string[] varType = { "String", "Float", "Bool" };
	
	public string[] stringOperation = { "Set to", "Append with" };
	public string[] boolOperation = { "Set to", "Invert" };
	public string[] floatOperation = { "Add", "Subtract", "Multiply by", "Divide by" };
	
	public VariableWindow( Dialog.DialogNode newNode )
	{
		node = newNode;
		windowName = "ID: " + newNode.id;
		node.type = Dialog.DialogNodeTypes.typeVariable;
		position = new Rect( node.x, node.y, 250, 200 );
	}
	
	public override void CreateNodeWindow(int id)
	{
		NodeHead();
		
		GUILayout.BeginVertical();
		
			GUILayout.BeginHorizontal();
			
				// BOX: VARIABLE
				GUILayout.Box ( "Variable:", GUILayout.ExpandWidth(true) );
				
				// BUTTON: SELECT INITIAL
				GUI.color = Color.blue;
				if ( GUILayout.Button( "", GUILayout.MaxWidth(15.0f) ) )
				{
					DialogWindow.Instance.SelectedInitial( node.id );
				}
				GUI.color = defaultColor;
			
			GUILayout.EndHorizontal();
			
			GUILayout.Space( 10.0f );
			
			// GLOBAL/LOCAL TOOLBAR
			node.variable.scope = GUILayout.Toolbar ( node.variable.scope, globalLocal );
			
			// VARIABLE TYPE TOOLBAR
			node.variable.variableType = GUILayout.Toolbar ( node.variable.variableType, varType );
			
			// BOX: CURRENT VALUE
			GUILayout.Box ( "Value:", GUILayout.ExpandWidth(true) );
			
			string[] variableNames = new string[0];
			if ( node.variable.scope == 0 )
			{	
				// string
				if ( node.variable.variableType == 0 )
				{
					variableNames = new string[ DialogWindow.Instance.dialogInst.variables.globals.gblStrNames.Count ];
					
					int index = 0;
					foreach ( string name in DialogWindow.Instance.dialogInst.variables.globals.gblStrNames )
					{
						variableNames[ index ] = name;
						index++;
					}	
				}
				// float
				else if ( node.variable.variableType == 1 )
				{
					variableNames = new string[ DialogWindow.Instance.dialogInst.variables.globals.gblFloatNames.Count ];
					
					int index = 0;
					foreach ( string name in DialogWindow.Instance.dialogInst.variables.globals.gblFloatNames )
					{
						variableNames[ index ] = name;
						index++;
					}
				}
				// bool
				else if ( node.variable.variableType == 2 )
				{
					variableNames = new string[ DialogWindow.Instance.dialogInst.variables.globals.gblBoolNames.Count ];
					
					int index = 0;
					foreach ( string name in DialogWindow.Instance.dialogInst.variables.globals.gblBoolNames )
					{
						variableNames[ index ] = name;
						index++;
					}
				}
			}
			
			else
			{				
				// string
				if ( node.variable.variableType == 0 )
				{
					variableNames = new string[ DialogWindow.Instance.dialogInst.dialog.stringNames.Count ];
					
					int index = 0;
					foreach ( string name in DialogWindow.Instance.dialogInst.dialog.stringNames )
					{
						variableNames[ index ] = name;
						index++;
					}
				}
				// float
				else if ( node.variable.variableType == 1 )
				{
					variableNames = new string[ DialogWindow.Instance.dialogInst.dialog.floatNames.Count ];
					
					int index = 0;
					foreach ( string name in DialogWindow.Instance.dialogInst.dialog.floatNames )
					{
						variableNames[ index ] = name;
						index++;
					}
				}
				// bool
				else if ( node.variable.variableType == 2 )
				{
					variableNames = new string[ DialogWindow.Instance.dialogInst.dialog.boolNames.Count ];
					
					int index = 0;
					foreach ( string name in DialogWindow.Instance.dialogInst.dialog.boolNames )
					{
						variableNames[ index ] = name;
						index++;
					}
				}
			}
				
			// string
			if ( node.variable.variableType == 0 )
			{				
				node.variable.variableOne = EditorGUILayout.Popup( node.variable.variableOne, variableNames );
	
				// BOX: MODIFY
				GUILayout.Box ( "Modify:", GUILayout.ExpandWidth(true) );
				
				node.variable.condition = GUILayout.Toolbar ( node.variable.condition, stringOperation );
			}
			
			// float
			else if ( node.variable.variableType == 1 )
			{
				node.variable.variableOne = EditorGUILayout.Popup( node.variable.variableOne, variableNames );
				
				// BOX: MODIFY
				GUILayout.Box ( "Modify:", GUILayout.ExpandWidth(true) );
				
				node.variable.condition = GUILayout.SelectionGrid ( node.variable.condition, floatOperation, 2 );
			}
			
			// bool
			else if ( node.variable.variableType == 2 )
			{
				node.variable.variableOne = EditorGUILayout.Popup( node.variable.variableOne, variableNames );
				
				// BOX: MODIFY
				GUILayout.Box ( "Modify:", GUILayout.ExpandWidth(true) );
				
				node.variable.condition = GUILayout.Toolbar ( node.variable.condition, boolOperation );
			}
			
			// this acts like an AND, peculiar as fuck.
			if ( node.variable.variableType != 2 || node.variable.condition != 1 )
			{
				node.variable.writeInVariable = EditorGUILayout.TextField( node.variable.writeInVariable );
			}			
			
			GUILayout.Space( 10.0f );
		
		GUILayout.EndVertical();
		
		node.x = position.x;
		node.y = position.y;
		GUI.DragWindow();
	}
}
