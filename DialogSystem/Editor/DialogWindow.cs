﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

public class DialogWindow : EditorWindow {
	
	/*--------------------------------------------------------------------
	
		D I A L O G   W I N D O W   C L A S S
		
			Everything from here belongs specifically to this
			DialogWindow class, and not a nested class

	*///------------------------------------------------------------------
	
	// Variables For: DialogWindow
	public Dialog dialogInst;
	public GUISkin customSkin;
	public static DialogWindow Instance { get; private set; }
	public Vector2 scrollPosition;
	private int multiplier = 1;
	
	// Variables For: NodeWindows
	Dictionary<int, NodeWindow> nodeWindows = new Dictionary<int, NodeWindow>();
	string[] nodeType = { "Text", "Choice", "Condition", "Variable" };
	int index;
	
	// Variables For: Node Connections
	int selectedInitialID = -1;
	int selectedAnswerIndex = -1;
	int selectedNextID = -1;
	
	// Variables For: ChoiceNode Answer Deletion
	Dialog.AnswerNode selectedAnswerToRemove = null;
	int selectedIndex = -1;
	
	// Variables For: VariableEditor
	Vector2 variableEditorScroll = new Vector2( 0, 0 );
	bool variableEditor = false;
	string varValue = "", varName = "", toRemove = "";
	
	int globalLocalIndex = 0;
	int varTypeIndex = 0;
	string[] globalLocal = { "Global", "Local" };
	string[] varType = { "String", "Float", "Bool" };
	
	public static bool IsOpen {
		get { return Instance != null; }
	}
	
	void OnEnable()
	{
		Instance = this;
		customSkin = Resources.Load<GUISkin> ("WindowSkin");
	}
	
	/*--------------------------------------------------------------------
		SHOW EDITOR
		
			MonoBehavior that runs when object is initialized

	*///------------------------------------------------------------------
	[MenuItem ("Window/Dialog Window")]
	public static void ShowEditor(Dialog activeDialog)
	{
		DialogWindow editor = EditorWindow.GetWindow<DialogWindow>();
		activeDialog.LoadDialog();
		
		editor.dialogInst = activeDialog;
		editor.NewLoad ();
	}
	
	/*--------------------------------------------------------------------
		NEW LOAD
		
			MonoBehavior that runs when object is initialized

	*///------------------------------------------------------------------
	void NewLoad()
	{
		foreach ( Dialog.DialogNode node in dialogInst.dialog.nodes )
		{
			AddNode ( node );
		}
	}
	
	/*--------------------------------------------------------------------
		SHOW WINDOWS
		
			MonoBehavior that runs when object is initialized

	*///------------------------------------------------------------------
	void ShowWindows()
	{
		foreach ( KeyValuePair<int, NodeWindow> pair in nodeWindows )
		{
			pair.Value.position = pair.Value.Show();
		}
	}
	
	/*--------------------------------------------------------------------
		ON GUI
		
			MonoBehavior that runs when object is initialized

	*///------------------------------------------------------------------
	void OnGUI()
	{			
		GUI.skin = customSkin;
		
		// WINDOW MENU
		GUILayout.BeginHorizontal( "box", GUILayout.Width(300) );
			
			index = EditorGUILayout.Popup( index, nodeType );
			
			// BUTTON: SAVE
			if ( GUILayout.Button("Save") )
			{
				Save();
			}
			
			// BUTTON: NEW NODE
			if ( GUILayout.Button( "Add Node" ) )
			{
				AddNewNode( index );
			}
			
			// BUTTON: OPEN VARIABLE EDITOR
			if ( GUILayout.Button( "Open Variable Editor" ) )
			{
				if ( variableEditor == true )
					variableEditor = false;
				else
					variableEditor = true;
			}
			
			GUILayout.Space( 10 );
			
			if ( GUILayout.Button ( "Expand window size" ) )
			{
				multiplier++;
			}
			
			if ( GUILayout.Button ( "Contract window size" ) )
			{
				if ( multiplier > 1 )
				{
					multiplier--;
				}
			}
		
		GUILayout.EndHorizontal();
		// END WINDOW MENU
		
		// SCROLLABLE VIEW
		int x = 0;
		if ( variableEditor )
		{
			x = 250;
			ShowVariableEditor();
		}
		else
			x = 0;
		
		scrollPosition = GUI.BeginScrollView( 
								new Rect(x, 30, position.width - x, position.height - 30), 
								scrollPosition, 
								new Rect(0, 0, position.width + 1000*multiplier, position.height + 1000*multiplier) );
		
		Color temp = GUI.color;
		GUI.color = Color.grey;
		
		GUI.Box ( new Rect(0, 0, position.width + 1000*multiplier, position.height + 1000*multiplier), "" );
		GUI.color = temp;

		if ( dialogInst != null && nodeWindows.Count > 0 )
		{
			BeginWindows();
			ShowWindows();
			DrawConnections();
			EndWindows();
			
			UpdateAnswers();
		}
		
		GUI.EndScrollView();
		
		// END SCROLLABLE VIEW
		
		GUI.skin = null;
	}
	
	
	/*--------------------------------------------------------------------
		SAVE
		
			MonoBehavior that runs when object is initialized

	*///------------------------------------------------------------------
	void Save()
	{
		dialogInst.WriteXML (dialogInst.dialog, dialogInst.xmlFilename);
		dialogInst.variables.Save ();
		
		AssetDatabase.Refresh ();
	}
	
	/*--------------------------------------------------------------------
		ADD NODE
		
			MonoBehavior that runs when object is initialized

	*///------------------------------------------------------------------
	public void AddNode( Dialog.DialogNode node )
	{
		switch ( node.type )
		{
			case Dialog.DialogNodeTypes.typeText:
			{
				nodeWindows.Add ( node.id, new TextWindow( node ) );
				break;
			}
			
			case Dialog.DialogNodeTypes.typeChoice:
			{
				nodeWindows.Add ( node.id, new ChoiceWindow( node ) );
				break;
			}
			
			case Dialog.DialogNodeTypes.typeStart:
			{
				nodeWindows.Add ( node.id, new StartWindow( node ) );
				break;
			}
			
			case Dialog.DialogNodeTypes.typeCondition:
			{
				nodeWindows.Add ( node.id, new ConditionWindow( node ) );
				break;
			}
			
			case Dialog.DialogNodeTypes.typeVariable:
			{
				nodeWindows.Add ( node.id, new VariableWindow( node ) );
				break;
			}
			
		default:
			{
				break;
			}
		}
	}
	
	/*--------------------------------------------------------------------
		ADD NEW NODE
		
			Determines the type of node requested and then
			creates a new node and adds it to the collection of nodeWindows

	*///------------------------------------------------------------------
	void AddNewNode( int index )
	{
		int windowBuffer = 25;
		switch ( index )
		{
			// text
			case 0:
			{
				Dialog.DialogNode newNode = dialogInst.dialog.createNewNode( Dialog.DialogNodeTypes.typeText );
				newNode.x = scrollPosition.x + windowBuffer;
				newNode.y = scrollPosition.y + windowBuffer;
				nodeWindows.Add( newNode.id, new TextWindow( newNode ) );
				break;
			}
			
			// choice
			case 1:
			{
				Dialog.DialogNode newNode = dialogInst.dialog.createNewNode( Dialog.DialogNodeTypes.typeChoice );
				newNode.x = scrollPosition.x + windowBuffer;
				newNode.y = scrollPosition.y + windowBuffer;
				nodeWindows.Add( newNode.id, new ChoiceWindow( newNode ) );
				break;
			}
			
			// condition
			case 2:
			{
				Dialog.DialogNode newNode = dialogInst.dialog.createNewNode( Dialog.DialogNodeTypes.typeCondition );
				newNode.x = scrollPosition.x + windowBuffer;
				newNode.y = scrollPosition.y + windowBuffer;
				nodeWindows.Add( newNode.id, new ConditionWindow( newNode ) );
				break;
			}
			
			// variable
			case 3:
			{
				Dialog.DialogNode newNode = dialogInst.dialog.createNewNode( Dialog.DialogNodeTypes.typeVariable );
				newNode.x = scrollPosition.x + windowBuffer;
				newNode.y = scrollPosition.y + windowBuffer;
				nodeWindows.Add( newNode.id, new VariableWindow( newNode ) );
				break;
			}
			
			default:
			{
				break;
			}
		}
	}
	
	/*--------------------------------------------------------------------
		REMOVE NODE

	*///------------------------------------------------------------------
	public void RemoveNode( NodeWindow window ) 
	{
		dialogInst.dialog.nodes.Remove ( window.node );
		dialogInst.dialog.nodeCollection.Remove ( window.node.id );
		nodeWindows.Remove( window.node.id );
		
		window = null;
	}
	
	/*--------------------------------------------------------------------
		SELECTED IN

	*///------------------------------------------------------------------
	public void SelectedNext( int newID ) 
	{
		selectedNextID = newID;
		AddConnection();
	}
	
	/*--------------------------------------------------------------------
		SELECTED OUT

	*///------------------------------------------------------------------
	public void SelectedInitial( int newID ) 
	{
		selectedInitialID = newID;
		AddConnection();
	}
	
	/*--------------------------------------------------------------------
		SELECTED ANSWER

	*///------------------------------------------------------------------
	public void SelectedAnswer( int newID, int newIndex )
	{
		selectedInitialID = newID;
		selectedAnswerIndex = newIndex;
		AddConnection();
	}
	
	/*--------------------------------------------------------------------
		SELECTED OUT
		
			Provides the ID and AnswerNode that have been selected for removal

	*///------------------------------------------------------------------
	public void SelectedAnswerToRemove( int newID, Dialog.AnswerNode answer )
	{
		selectedIndex = newID;
		selectedAnswerToRemove = answer;
	}
	
	/*--------------------------------------------------------------------
		UPDATE ANSWERS
		
			Performs update operations for the Answer Nodes

	*///------------------------------------------------------------------
	void UpdateAnswers()
	{
		if ( selectedIndex != -1 && selectedAnswerToRemove != null )
		{
			dialogInst.dialog.nodeCollection[ selectedIndex ].answers.Remove( selectedAnswerToRemove );
			selectedIndex = -1;
			selectedAnswerToRemove = null;
		}
	}
	
	/*--------------------------------------------------------------------
		ADD CONNECTION
		
			Adds a connection between the two selected nodes.

	*///------------------------------------------------------------------
	void AddConnection()
	{
		if ( selectedInitialID != -1 && selectedNextID != -1 )
		{
			Dialog.DialogNodeTypes initialNodetype = dialogInst.dialog.nodeCollection[ selectedInitialID ].type;
		
			/*--------------------------------------------------------------------
				Checks if connection is between a choice node's parent and child
			*///------------------------------------------------------------------
			if ( selectedInitialID == selectedNextID && initialNodetype == Dialog.DialogNodeTypes.typeChoice )
			{
				dialogInst.dialog.nodeCollection[ selectedInitialID ].answers[ selectedAnswerIndex ].nodeOut = -1;
			}
			
			/*--------------------------------------------------------------------
				Checks if connection is between a variable node's parent and child
			*///------------------------------------------------------------------
			else if ( selectedInitialID == selectedNextID && initialNodetype == Dialog.DialogNodeTypes.typeCondition )
			{
				dialogInst.dialog.nodeCollection[ selectedInitialID ].variable.nodeOut[ selectedAnswerIndex ] = -1;
			}
			
			/*--------------------------------------------------------------------
				Handles all other connections between child and parent
			*///------------------------------------------------------------------
			else if ( selectedInitialID == selectedNextID )
			{
				dialogInst.dialog.nodeCollection[ selectedInitialID ].nodeOut = -1;
			}
			
			// Adding connections
			else if ( dialogInst.dialog.nodeCollection[ selectedInitialID ].type == Dialog.DialogNodeTypes.typeChoice )
			{
				dialogInst.dialog.nodeCollection[ selectedInitialID ].answers[ selectedAnswerIndex ].nodeOut = selectedNextID;
			}
			
			else if ( dialogInst.dialog.nodeCollection[ selectedInitialID ].type == Dialog.DialogNodeTypes.typeCondition )
			{
				dialogInst.dialog.nodeCollection[ selectedInitialID ].variable.nodeOut[ selectedAnswerIndex ] = selectedNextID;
			}
			
			else
			{
				dialogInst.dialog.nodeCollection[ selectedInitialID ].nodeOut = selectedNextID;
			}
			
			selectedInitialID = -1;
			selectedAnswerIndex = -1;
			selectedNextID = -1;
		}
	}
	
	
	/*--------------------------------------------------------------------
		DRAW CONNECTIONS
		
			MonoBehavior that runs when object is initialized

	*///------------------------------------------------------------------
	void DrawConnections()
	{
		foreach ( KeyValuePair<int, NodeWindow> pair in nodeWindows )
		{
			if ( pair.Value.node.type == Dialog.DialogNodeTypes.typeChoice )
			{
				foreach ( Dialog.AnswerNode answer in pair.Value.node.answers )
				{
					if ( !nodeWindows.ContainsKey( answer.nodeOut ) )
					{
						answer.nodeOut = -1;
					}
					else if ( answer.nodeOut != -1 )
					{
						Rect temp = new Rect( answer.x, answer.y, 10, 10 );
						DrawNodeCurve( temp, nodeWindows[ answer.nodeOut ].position );
					}
				}
			}
			
			else if ( pair.Value.node.type == Dialog.DialogNodeTypes.typeCondition )
			{
				for ( int i = 0; i < pair.Value.node.variable.nodeOut.Count; i++ )
				{
					
					if ( !nodeWindows.ContainsKey( pair.Value.node.variable.nodeOut[i] ) )
					{
						pair.Value.node.variable.nodeOut[i] = -1;
					}
					
					else if ( pair.Value.node.variable.nodeOut[i] != -1 )
					{
						Rect temp = new Rect( pair.Value.node.variable.coordinates[i].x, 
						                     pair.Value.node.variable.coordinates[i].y, 
						                     10, 10 );
						DrawNodeCurve( temp, nodeWindows[ pair.Value.node.variable.nodeOut[i] ].position );
					}
					
				}
			}
			
			else
			{
				if ( !nodeWindows.ContainsKey( pair.Value.node.nodeOut ) )
				{
					pair.Value.node.nodeOut = -1;
				}
				
				if ( pair.Value.node.nodeOut != -1 )
				{
					// nodeWindows[ pair.Value.node.id ].position changed to pair.Value.position
					DrawNodeCurve( pair.Value.position, nodeWindows[ pair.Value.node.nodeOut ].position );	
				}
			}
		}
	}
	
	/*--------------------------------------------------------------------
		DRAW NODE CURVE
		
			Draws a bezier curve connecting two Rects

	*///------------------------------------------------------------------
	void DrawNodeCurve(Rect start, Rect end) 
	{
		Vector3 startPos = new Vector3(start.x + start.width, start.y + start.height / 2, 0);
		Vector3 endPos = new Vector3(end.x, end.y + end.height / 2, 0);
		
		float distance = Mathf.Clamp( (end.x - 10) - (start.x + start.width), -500, 100);
		
		if ( distance < 0 )
			distance *= -1;
		
		Vector3 startTan = startPos + Vector3.right * distance;
		Vector3 endTan = endPos + Vector3.left * distance;
		
		Color shadowCol = new Color(0, 0, 0, 0.06f);
		
		for (int i = 0; i < 3; i++) // Draw a shadow	
			Handles.DrawBezier(startPos, endPos, startTan, endTan, shadowCol, null, (i + 1) * 5);
		
		Handles.DrawBezier(startPos, endPos, startTan, endTan, Color.black, null, 1);
	}
	
	/*--------------------------------------------------------------------
		SHOW VARIABLE EDITOR
		
			MonoBehavior that runs when object is initialized

	*///------------------------------------------------------------------
	void ShowVariableEditor()
	{				
		int scrollViewMultiplier = 1;
		
		GUILayout.BeginVertical ( GUILayout.Height(50), GUILayout.Width(250) );
		
			// BOX: VARIABLE EDITOR
			GUILayout.Box ( "Variable Editor", GUILayout.ExpandWidth(true) );
			
			// TOOLBAR: GLOBAL/LOCAL
			globalLocalIndex = GUILayout.Toolbar ( globalLocalIndex, globalLocal );
			
			// TOOLBAR: VARIABLE TYPE
			varTypeIndex = GUILayout.Toolbar ( varTypeIndex, varType );
			
			// VARIABLE NAME
			GUILayout.BeginHorizontal();
				GUILayout.Label ( "Name", GUILayout.MaxWidth(60) );
				varName = GUILayout.TextField ( varName );
			GUILayout.EndHorizontal();
			
			// VARIABLE VALUE
			GUILayout.BeginHorizontal();
				GUILayout.Label ( "Value", GUILayout.MaxWidth(60) );
				varValue = GUILayout.TextField ( varValue );
			GUILayout.EndHorizontal();
			
			if ( GUILayout.Button( "Add New Variable" ) )
			{
				if ( globalLocalIndex == 0 )
				{	
					// string
					if ( varTypeIndex == 0 )
					{
						DialogWindow.Instance.dialogInst.variables.globals.gblStrNames.Add ( varName );
						DialogWindow.Instance.dialogInst.variables.globals.gblStrValues.Add ( varValue );
					}
					// float
					else if ( varTypeIndex == 1 )
					{
						DialogWindow.Instance.dialogInst.variables.globals.gblFloatNames.Add ( varName );
						DialogWindow.Instance.dialogInst.variables.globals.gblFloatValues.Add ( float.Parse(varValue) );
					}
				// bool
					else if ( varTypeIndex == 2 && varValue == "true" || varValue == "false" )
					{
						DialogWindow.Instance.dialogInst.variables.globals.gblBoolNames.Add ( varName );
						DialogWindow.Instance.dialogInst.variables.globals.gblBoolValues.Add ( bool.Parse(varValue) );
					}
				}
				
				// local
				else
				{				
					// string
					if ( varTypeIndex == 0 )
					{
						DialogWindow.Instance.dialogInst.dialog.stringNames.Add ( varName );
						DialogWindow.Instance.dialogInst.dialog.stringValues.Add ( varValue );
					}
					// float
					else if ( varTypeIndex == 1 )
					{
						DialogWindow.Instance.dialogInst.dialog.floatNames.Add ( varName );
						DialogWindow.Instance.dialogInst.dialog.floatValues.Add ( float.Parse(varValue) );
					}
					// bool
					else if ( varTypeIndex == 2 )
					{
						DialogWindow.Instance.dialogInst.dialog.boolNames.Add ( varName );
						DialogWindow.Instance.dialogInst.dialog.boolValues.Add ( bool.Parse(varValue) );
					}
				}
			}
			
		GUILayout.Label ( "Remove variable with name: " );
		toRemove = GUILayout.TextField ( toRemove );
		
		if ( GUILayout.Button ( "Remove Variable" ) )
		{
			try
			{
				int indexToRmv;
				switch ( varTypeIndex )
				{
				
				// string
				case 0:
					switch ( globalLocalIndex )
					{
					case 0:
						indexToRmv = DialogWindow.Instance.dialogInst.variables.globals.gblStrNames.IndexOf ( toRemove );
						DialogWindow.Instance.dialogInst.variables.globals.gblStrNames.RemoveAt ( indexToRmv );
						DialogWindow.Instance.dialogInst.variables.globals.gblStrValues.RemoveAt ( indexToRmv );
						
						scrollViewMultiplier = DialogWindow.Instance.dialogInst.variables.globals.gblStrNames.Count;
						break;
						
					case 1:
						indexToRmv = DialogWindow.Instance.dialogInst.dialog.stringNames.IndexOf ( toRemove );
						DialogWindow.Instance.dialogInst.dialog.stringNames.RemoveAt ( indexToRmv );
						DialogWindow.Instance.dialogInst.dialog.stringValues.RemoveAt ( indexToRmv );
						
						scrollViewMultiplier = DialogWindow.Instance.dialogInst.dialog.stringNames.Count;
						break;
					}
					break;
					
				// float
				case 1:
					switch ( globalLocalIndex )
					{
					case 0:
						indexToRmv = DialogWindow.Instance.dialogInst.variables.globals.gblStrNames.IndexOf ( toRemove );
						DialogWindow.Instance.dialogInst.variables.globals.gblFloatNames.RemoveAt ( indexToRmv );
						DialogWindow.Instance.dialogInst.variables.globals.gblFloatValues.RemoveAt ( indexToRmv );
						
						scrollViewMultiplier = DialogWindow.Instance.dialogInst.variables.globals.gblFloatNames.Count;
						break;
						
					case 1:
						indexToRmv = DialogWindow.Instance.dialogInst.dialog.floatNames.IndexOf ( toRemove );
						DialogWindow.Instance.dialogInst.dialog.floatNames.RemoveAt ( indexToRmv );
						DialogWindow.Instance.dialogInst.dialog.floatValues.RemoveAt ( indexToRmv );
						
						scrollViewMultiplier = DialogWindow.Instance.dialogInst.dialog.floatNames.Count;
						break;
					}
					break;
					
				// bool
				case 2:
					switch ( globalLocalIndex )
					{
					case 0:
						indexToRmv = DialogWindow.Instance.dialogInst.variables.globals.gblBoolNames.IndexOf ( toRemove );
						DialogWindow.Instance.dialogInst.variables.globals.gblBoolNames.RemoveAt ( indexToRmv );
						DialogWindow.Instance.dialogInst.variables.globals.gblBoolValues.RemoveAt ( indexToRmv );
						
						scrollViewMultiplier = DialogWindow.Instance.dialogInst.variables.globals.gblBoolNames.Count;
						break;		
						
					case 1:
						indexToRmv = DialogWindow.Instance.dialogInst.dialog.boolNames.IndexOf ( toRemove );
						DialogWindow.Instance.dialogInst.dialog.boolNames.RemoveAt ( indexToRmv );
						DialogWindow.Instance.dialogInst.dialog.boolValues.RemoveAt ( indexToRmv );
						
						scrollViewMultiplier = DialogWindow.Instance.dialogInst.dialog.boolNames.Count;
						break;
					}
					break;
				}
			}
			catch( System.ArgumentOutOfRangeException )
			{
				Debug.Log ( "No variable with this name found.");
			}
			
		}
				
		GUILayout.EndVertical();
				
		variableEditorScroll = GUI.BeginScrollView( 
		                                           new Rect(0, 220, 250, 300), 
		                                           variableEditorScroll,
		                                           new Rect(0, 220, 200, 400*scrollViewMultiplier) );
		
		Color temp = GUI.color;
		GUI.color = Color.grey;
		GUI.Box ( new Rect(0, 220, 250, position.height + 500), "" );
		GUI.color = temp;
		
		GUILayout.BeginVertical();
				
			if ( globalLocalIndex == 0 )
			{	
				// string
				if ( varTypeIndex == 0 )
				{
					for ( int i = 0; i < DialogWindow.Instance.dialogInst.variables.globals.gblStrNames.Count; i++ )
					{
						GUILayout.Box ( DialogWindow.Instance.dialogInst.variables.globals.gblStrNames[i]
										+ " " + DialogWindow.Instance.dialogInst.variables.globals.gblStrValues[i]
										, GUILayout.Width( 230 ) );
					}
				}
				// float
				else if ( varTypeIndex == 1 )
				{
					for ( int i = 0; i < DialogWindow.Instance.dialogInst.variables.globals.gblFloatNames.Count; i++ )
					{
						GUILayout.Box ( DialogWindow.Instance.dialogInst.variables.globals.gblFloatNames[i]
						               + " " + DialogWindow.Instance.dialogInst.variables.globals.gblFloatValues[i]
						               , GUILayout.Width( 230 ) );
					}
				}
				// bool
				else if ( varTypeIndex == 2 )
				{
					for ( int i = 0; i < DialogWindow.Instance.dialogInst.variables.globals.gblBoolNames.Count; i++ )
					{
						GUILayout.Box ( DialogWindow.Instance.dialogInst.variables.globals.gblBoolNames[i]
						               + " " + DialogWindow.Instance.dialogInst.variables.globals.gblBoolValues[i]
						               , GUILayout.Width( 230 ) );
					}
				}
			}
			
			// local
			else
			{				
				// string
				if ( varTypeIndex == 0 )
				{
					for ( int i = 0; i < DialogWindow.Instance.dialogInst.dialog.stringNames.Count; i++ )
					{
						GUILayout.Box ( DialogWindow.Instance.dialogInst.dialog.stringNames[i] + " " 
										+ DialogWindow.Instance.dialogInst.dialog.stringValues[i]
										, GUILayout.Width( 230 ) );
					}
				}
				// float
				else if ( varTypeIndex == 1 )
				{
					for ( int i = 0; i < DialogWindow.Instance.dialogInst.dialog.floatNames.Count; i++ )
					{
						GUILayout.Box ( DialogWindow.Instance.dialogInst.dialog.floatNames[i] + " " 
						               + DialogWindow.Instance.dialogInst.dialog.floatValues[i]
						               , GUILayout.Width( 230 ) );
					}
				}
				// bool
				else if ( varTypeIndex == 2 )
				{
					for ( int i = 0; i < DialogWindow.Instance.dialogInst.dialog.boolNames.Count; i++ )
					{
						GUILayout.Box ( DialogWindow.Instance.dialogInst.dialog.boolNames[i] + " " 
						               + DialogWindow.Instance.dialogInst.dialog.boolNames[i]
						               , GUILayout.Width( 230 ) );
					}
				}
			}
		
		GUILayout.EndVertical();		
		
		GUI.EndScrollView();
	}
	
}
