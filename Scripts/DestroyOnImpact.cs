﻿using UnityEngine;
using System.Collections;

public class DestroyOnImpact : MonoBehaviour {
	public GameObject[] destroyAlso;

	void OnCollisionEnter( Collision other ) {
		foreach(GameObject obj in destroyAlso) 
			Destroy (obj);
		Destroy (gameObject);
	}
}
