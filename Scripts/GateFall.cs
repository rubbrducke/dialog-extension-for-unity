﻿using UnityEngine;
using System.Collections;

public class GateFall : MonoBehaviour {
	private bool pressedButton = false;

	public GameObject target;
	void onMouseOver() {
		pressedButton = true;
	}
	void onMouseExit() {
		pressedButton = false;
	}
	void onMouseDown() {
		target = GameObject.Find ("Gate");
		target.GetComponent<Animation>().Play ("fallover");
	}
	void onGUI() {
		if (pressedButton == true) {
			GUI.Box(new Rect(300,200,200,20), "Use E or whatever to attack");
		}
	}
}
