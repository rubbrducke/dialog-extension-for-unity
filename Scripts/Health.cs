﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

	public int totalHealth = 15;
	public int currentHealth = 15;
	
	GameObject player;
	
	bool retryScreen = false;
	bool dead = false;
	
	void OnEnable()
	{
		dead = false;
		currentHealth = totalHealth;
	}
	
	void Update()
	{
		if ( Time.timeSinceLevelLoad < 1 )
		{
			dead = false;
			retryScreen = false;
			currentHealth = totalHealth;
		}
	}
	
	void OnGUI()
	{
		if ( retryScreen )
		{
			var buttonRect = new Rect( 10, 10, 100, 40 );
			buttonRect.center = new Vector2( Screen.width/2, Screen.height/2 );
			
			if ( GUI.Button( buttonRect, "Retry?" ) )
			{
				respawn();
			}
		}
	}
	
	public void respawn()
	{
		retryScreen = false;
		Time.timeScale = 1.0f;
		
		// respawn player
		gameObject.GetComponent<Respawnable>().RespawnObject();
		
		dead = false;
		retryScreen = false;
		currentHealth = totalHealth;
	}
	
	public int takeDamage( int damage )
	{
		currentHealth -= damage;
		
		if ( currentHealth <= 0 && !dead )
		{
			currentHealth = 0;
			
			// if dead object is the player
			if (gameObject.name == "Player")
			{
				dead = true;
				var robotAnim = gameObject.transform.FindChild ("roBot");
				robotAnim.GetComponent<Animation>().Play("final_head");
			
				Time.timeScale = 0.0f;
				retryScreen = true;
				
				Debug.Log( "Health: set timescale to 0" );
			}
			
			// if dead object is an enemy of the state
			if ( gameObject.tag == "Enemy" )
			{
				dead = true;
				gameObject.GetComponent<Animation>().Play("Death");
				
				Destroy( gameObject.GetComponent<FollowPlayer>() );
				Destroy( gameObject.GetComponent<DamageOnTouch>() );
				Destroy( gameObject.GetComponent<Rigidbody>() );
				Destroy( gameObject.GetComponent<Collider>() );
				Destroy( this );
			}
		}
		
		return currentHealth;
	}
}
