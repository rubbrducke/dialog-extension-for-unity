﻿using UnityEngine;
using System.Collections;

public class ThrowObjectPlayer : MonoBehaviour {

	public float throwPower = 1000;
	public GameObject obj; 
	public float throwAngle = 0;
	public GameObject cameraObject;
	
	void Start () {
	
	}

	void Update () {
		ThrowObj();
	}

	void ThrowObj() {
		if(Input.GetMouseButtonDown(0)) {
			//Creates the throw GameObject
			Vector3 throwPosition = new Vector3(
				transform.position.x,
				transform.GetComponent<Collider>().bounds.max.y * 0.5f,
				transform.position.z
			);
			throwPosition += transform.forward*0.6f;
			GameObject newObj = Instantiate(obj, 
				throwPosition,
				transform.rotation
			) as GameObject;  

			//Add the force to the throw object
			Vector3 dir = Quaternion.AngleAxis (throwAngle, -transform.right) * cameraObject.transform.forward;
			newObj.GetComponent<Rigidbody>().AddForce(dir*throwPower);
		}
	}

}
