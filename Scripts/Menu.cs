﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour 
{
	public string levelOneName;
	public string levelTutorName;
		
	void OnGUI()
	{
		var rect = new Rect( 10, 10, 200, 50 );
		rect.center = new Vector2(  Screen.width/2, Screen.height/2 - 70 );
		
		if ( GUI.Button( rect, "Play Level" ) )
		{
			Application.LoadLevel( levelOneName );
		}
		
		rect.center = new Vector2( rect.center.x, rect.center.y + 70 );
		
		if ( GUI.Button( rect, "Play Tutorial" ) )
		{
			Application.LoadLevel ( levelTutorName );
		}
		
		rect.center = new Vector2( rect.center.x, rect.center.y + 70 );
		
		if ( GUI.Button ( rect, "Exit Game" ) )
		{
			Application.Quit();
		}
	}
}
