﻿using UnityEngine;
using System.Collections;

public class MovingPlatform : MonoBehaviour {

	public Transform platform;
	public Transform startTransform;
	public Transform endTransform;
	public float platformSpeed;
	public float waitDuration;

	Vector3 direction;
	Transform destination;

	void Start()
	{
		SetDestination (endTransform); //Start moving toward the end position
	}

	void FixedUpdate()
	{
		platform.GetComponent<Rigidbody>().MovePosition (platform.position + direction * platformSpeed * Time.fixedDeltaTime);
		if (Vector3.Distance (platform.position, destination.position) < platformSpeed * Time.fixedDeltaTime) 
		{
			SetDestination(destination == startTransform ? endTransform : startTransform);
		}

	}

	void OnDrawGizmos() //Only for purposes of development. Shows start position as green and end position as red
	{
		Gizmos.color = Color.green;
		Gizmos.DrawWireCube (startTransform.position, platform.localScale);

		Gizmos.color = Color.red;
		Gizmos.DrawWireCube (endTransform.position, platform.localScale);
	}

	void SetDestination(Transform dest) //Sets the destination of the platform and the direction it will move in
	{
		destination = dest;
		direction = (destination.position - platform.position).normalized;
	}
}
