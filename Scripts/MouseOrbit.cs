﻿using UnityEngine;
using System.Collections;

public class MouseOrbit : MonoBehaviour {
	public Transform target;
	public float distance = 6.0f;
	public float xSpeed = 150.0f;
	public float ySpeed = 150.0f;
	
	public float yMinLimit = -20f;
	public float yMaxLimit = 80f;
	
	public float distanceMin = 0f;
	public float distanceMax = 5f;

	public float headRatio = 1/8f;

	public float preferredDistance;

	public float smooth = 2.0f;

	float x = 0.0f;
	float y = 0.0f;
	
	// Use this for initialization
	void Start () {
		preferredDistance = distance;
		Vector3 angles = transform.eulerAngles;
		x = angles.y;
		y = angles.x;
		
		// Make the rigid body not change rotation
		if (GetComponent<Rigidbody>())
			GetComponent<Rigidbody>().freezeRotation = true;
	}
	
	void Update () {
		target = GameObject.Find ("Player").transform;

		if (target) {

			x += Input.GetAxis("Mouse X") * xSpeed * 0.02f;
			y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;

			y = ClampAngle(y, yMinLimit, yMaxLimit);
			
			Quaternion rotation = Quaternion.Euler(y, x, 0);
			
			distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel")*5, distanceMin, distanceMax);
			preferredDistance = Mathf.Clamp(preferredDistance - Input.GetAxis("Mouse ScrollWheel")*5, distanceMin, distanceMax);


//			//Use raycast to change camera distance if object is between camera and target
//			Vector3 relativePos = transform.collider.bounds.center - target.collider.bounds.center;
//			RaycastHit hit;
//			int layerMask = 1 << 8; //camera layer
//			layerMask = ~layerMask; //Will not cause collision with camera layer 
//
//			// If character moving in the positive Y direction, reverse the position and lower the distance
//			//Also try to find a sweet spot to prevent bobbing back and forth
//
//			if (Physics.Raycast (collider.bounds.min, new Vector3(0,-1,-1), out hit, 0.3f)) {
//				distance -= .2f;
//				//Reverse the rotation from y mouse
//			} else if (Physics.Raycast (target.collider.bounds.center, relativePos, out hit, distance, layerMask)) {
//				Debug.DrawLine(target.collider.bounds.center, hit.point);
//				distance = hit.distance;
//			} else {
//				if (distance < preferredDistance) {
//					distance += 0.03f;
//				}
//			}


			Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
			Vector3 targetHeadPosition = new Vector3(target.position.x, target.GetComponent<Collider>().bounds.max.y - target.GetComponent<Collider>().bounds.max.y*headRatio*.5f, target.position.z);
			Vector3 position = rotation * negDistance + targetHeadPosition;
		




			//rotate character (X only) on mouse movement
			target.rotation = Quaternion.Euler(0,x,0);
	
			//rotate camera on mouse movement
			transform.rotation = rotation;
//			transform.position = Vector3.Lerp(transform.position, position, Time.deltaTime * smooth);
			transform.position = position;

		}
		
	}
	
	public static float ClampAngle(float angle, float min, float max)
	{
		if (angle < -360F)
			angle += 360F;
		if (angle > 360F)
			angle -= 360F;
		return Mathf.Clamp(angle, min, max);
	}


}
