﻿using UnityEngine;
using System.Collections;

public class RespawnZone : MonoBehaviour 
{	
	void OnTriggerEnter ( Collider other )
	{
		if ( other.gameObject.tag == "Player" )
		{
			other.GetComponent<Respawnable>().currentRespawnLoc = gameObject.transform.position;
		}
	}
}
