﻿using UnityEngine;
using System.Collections;

public class VictoryCondition : MonoBehaviour 
{	
	bool winCond = false;
	CollectableTally finalTally;
	int totalPossibleTally;
	int finalTime;
	
	void Start()
	{
		totalPossibleTally = GameObject.FindGameObjectsWithTag("Coin").Length;
	}
	
	void OnTriggerEnter ( Collider other )
	{	
		if ( other.tag == "Player" )
		{
			winCond = true;
			finalTally = other.GetComponent<CollectableTally>();
			finalTime = (int) Time.time;
		}
	}
	
	void OnGUI()
	{
		if ( winCond )
		{
			Time.timeScale = 0;
			
			var rect = new Rect( 10, 10, 200, 60 );
			rect.center = new Vector2(  Screen.width/2, Screen.height/2 - 70 );
			
			string finalwords = "";
			
			if ( (finalTally.tally / totalPossibleTally) > 0 )
			{
				finalwords = "You Won! " + "\nFinal Score: " + finalTally.tally + "\nTime: " + finalTally.getFinalTime( finalTime );
			}
			else
			{
				finalwords = "You Lost! \nYou didn't get enough bolts.. " + "\nFinal Score: " + finalTally.tally + "\nTime: " + finalTally.getFinalTime( finalTime );
			}
			
			GUI.Box ( rect, finalwords );
			
			rect.height = 35;
			rect.center = new Vector2( rect.center.x, rect.center.y + 70 );
			
			if ( GUI.Button( rect, "Play again?" ) )
			{
				winCond = false;
				Time.timeScale = 1;
				finalTally.restart ();
				Application.LoadLevel( Application.loadedLevelName );
			}
			
			rect.center = new Vector2( rect.center.x, rect.center.y + 70 );
			
			if ( GUI.Button( rect, "Return to Main Menu" ) )
			{
				winCond = false;
				Time.timeScale = 1;
				Application.LoadLevel ( "MainMenu" );
			}
		}		
	}
}
