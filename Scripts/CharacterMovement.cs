﻿using UnityEngine;
using System.Collections;

public class CharacterMovement : MonoBehaviour {

	public float moveSpeed = 5.0F;
	public float turnSpeed = 7.5F;
	public float jumpForce = 7.5F;
	public float jumpGravity = 20.0F;
	public bool isTouchingGround;

	private Transform robot;

	private Vector3 moveVect;
	private Vector3 direction;
	private bool canMove;

	public int punchDamage;
	private bool leftHandPunch;

	private AudioSource[] audio;
	
	void Start() {
		isTouchingGround = false; //Collision detecting turns this true
		robot = transform.FindChild ("roBot");
		moveVect = Vector3.zero;
		direction = Vector3.forward;
		transform.rotation = Quaternion.LookRotation (Vector3.forward);

		canMove = true;
		leftHandPunch = true;

		if (punchDamage == null)
			punchDamage = 0;

		audio = Camera.main.GetComponents<AudioSource>();

	}
	
	void Update() {
		//Create raycast to see if character can move in direction character is facing
		Vector3 camPos = Camera.main.transform.position;
		camPos.y = transform.position.y;
		direction = -(camPos - transform.position).normalized;
	
		RaycastHit hit;
		if (Physics.Raycast (transform.GetComponent<Collider>().bounds.center, direction, out hit, 1)) {
			if (hit.collider.tag != "Switch" && hit.collider.tag != "Respawn" && hit.collider.tag != "Coin" ) //Switch colliders shouldnt prevent movement
				canMove = false;
			else
				canMove = true;
		} else {
			canMove = true;
		}
		/*
		RaycastHit hit2;
		if (Physics.Raycast (transform.collider.bounds.center, Vector3.right, out hit2, 1.5f)) {
			if (hit2.collider.tag != "Switch" && hit2.collider.tag != "Respawn") //Switch colliders shouldnt prevent movement
				canMove = false;
			else
				canMove = true;
		} else {
			canMove = true;
		}
		RaycastHit hit3;
		if (Physics.Raycast (transform.collider.bounds.center, -Vector3.right, out hit3, 1.5f)) {
			if (hit3.collider.tag != "Switch" && hit3.collider.tag != "Respawn") //Switch colliders shouldnt prevent movement
				canMove = false;
			else
				canMove = true;
		} else {
			canMove = true;
		}
		RaycastHit hit4;
		if (Physics.Raycast (transform.collider.bounds.center, -Vector3.forward, out hit4, 1.5f)) {
			if (hit4.collider.tag != "Switch" && hit4.collider.tag != "Respawn") //Switch colliders shouldnt prevent movement
				canMove = false;
			else
				canMove = true;
		} else {
			canMove = true;
		}*/
		KeyMovement ();

		if (!robotIsPunching())
			AnimateCharacter ();

		Attack ();

		Debug.DrawRay (transform.GetComponent<Collider>().bounds.center, direction, Color.red);
	}
	
	void KeyMovement() {

		//Basic movement transform
		moveVect = new Vector3(Input.GetAxis("Horizontal"),0,Input.GetAxis("Vertical"));
		

		if (canMove == false) {
			GetComponent<Rigidbody>().AddForce(-direction* Time.deltaTime * moveSpeed, ForceMode.Impulse);
		} else {
			moveVect *= (moveSpeed * Time.deltaTime); 
			transform.Translate( moveVect );
		}

		//Jumping
		if (Input.GetButton("Jump") && isTouchingGround) {
			robot.GetComponent<Animation>().Play ("jump");
			
			isTouchingGround = false;
			
			Physics.gravity = new Vector3(0, -jumpGravity, 0);
			GetComponent<Rigidbody>().AddForce(Vector3.up*jumpForce*GetComponent<Rigidbody>().mass, ForceMode.Impulse); 

		}
		
		//Prevent from sliding down hill or other terrain and prevent from climing steep hills
		if (isTouchingGround) {
			if ( moveVect.magnitude == 0){ 
				GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ |
					RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
			} else {
				GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
			}
		}
		
	}
	
	void RotateCharacter(Vector3 angle) {
		var fromAngle = robot.transform.localRotation;
		var toAngle = Quaternion.Euler(angle);
		robot.transform.localRotation = Quaternion.Lerp(fromAngle, toAngle, Time.deltaTime * turnSpeed);
	
	}
	
	//Rotates character appropriate direction and determines if an animation should be played
	void animate_jump_or_other(string animation_name, int angle) {
		if (Input.GetButtonDown("Attack1"))
		{
			robot.transform.localRotation = Quaternion.Euler (0, angle + 75, 0);
		}

		else if (Input.GetButtonDown("Jump"))
		{
			robot.transform.localRotation = Quaternion.Euler (0, angle + 30, 0);

		} 
		
		else if(isTouchingGround && name=="loop_idle") 
		{
			robot.transform.localRotation = Quaternion.Euler (0, angle + 30 ,0);
			robot.GetComponent<Animation>().Play ("loop_idle");
		} 
		
		else if(isTouchingGround) 
		{
			RotateCharacter(new Vector3(0, angle + 90, 0));
			robot.GetComponent<Animation>().Play (animation_name);
		} 

		direction = Quaternion.AngleAxis(angle, Vector3.up) * direction;
	}
	
	//Perform rotations and animations
	void AnimateCharacter() {


		//Moving Foward and Right
		if (Input.GetKey("d") && Input.GetKey("w")) {
			animate_jump_or_other("loop_run_funny", 45);
		}
		
		//Moving Forward and left
		else if (Input.GetKey("a") && Input.GetKey("w")) {
			animate_jump_or_other("loop_run_funny", 315);
		}
		
		//Moving Backward and Right
		else if (Input.GetKey("d") && Input.GetKey("s")) {
			animate_jump_or_other("loop_run_funny", 135);
		}
		
		//Moving Backward and left
		else if (Input.GetKey("a") && Input.GetKey("s")) {
			animate_jump_or_other("loop_run_funny", 225);
		}
	
		//Move Forward
		else if (Input.GetKey("w")) {
			animate_jump_or_other("loop_run_funny", 0);
		}

		//Moving Backward
		else if (Input.GetKey("s")) {
			animate_jump_or_other("loop_run_funny", 180);
		}

		//Moving Left
		else if (Input.GetKey("a")) {
			animate_jump_or_other("loop_run_funny", 270); 
		}

		//Moving Right
		else if (Input.GetKey("d")) {
			animate_jump_or_other("loop_run_funny", 90);
		}
	
		//Idle
		else {
			animate_jump_or_other("loop_idle", 0);
		}
	}
	
	void Attack() {
		// Punch on pressing Attack1 input
		if (Input.GetButtonDown("Attack1")) {
			if (!robotIsPunching()) {
				if (leftHandPunch) {
					robot.GetComponent<Animation>().Play ("punch_hi_left");
					leftHandPunch = false;
				} else { 
					robot.GetComponent<Animation>().Play("punch_hi_right");
					leftHandPunch = true;
				}
				audio[1].Play();
				//If the punch 'hits' anything with tag enemy, apply damage to target
				RaycastHit hit;
				if (Physics.Raycast (transform.GetComponent<Collider>().bounds.center, direction, out hit, 5)) {
					if (hit.collider.tag == "Enemy") {
						GameObject enemy = hit.transform.gameObject;

						//Check if target has the Health script attached
						if (enemy.GetComponent<Health> () != null) {

							enemy.GetComponent<Health> ().takeDamage(punchDamage);
							//Decrement health by damage, if health == 0, object dies
							//if (enemy.GetComponent<Health> ().takeDamage(punchDamage) == 0) {
								//If target has dealth animation, show it and destroy target after specific time
								//Destroy(target, time);
								
								//Remove target 
								//Destroy(enemy);
				
							//}
						}
					}
				}
			}
		}
	}

	bool robotIsPunching() {
		if (robot.GetComponent<Animation>().IsPlaying("punch_hi_left") || robot.GetComponent<Animation>().IsPlaying("punch_hi_right"))
			return true;
		else
			return false;
	}

	//Detect Ground Collision
	void OnCollisionEnter(Collision colliInfo) {
		if (colliInfo.gameObject.tag == "Ground") {  //Make sure to add the Ground Tag to GameObject
			isTouchingGround = true;
		}
		if (colliInfo.gameObject.tag == "movPlatform") 
		{
			transform.parent = colliInfo.transform;
			isTouchingGround = true;
		} else 
		{
			transform.parent = null;
		}
	}


}
