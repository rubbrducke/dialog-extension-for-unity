﻿using UnityEngine;
using System.Collections;

public class DamageOnTouch : MonoBehaviour {
	
	public int damage;
	
	void OnCollisionEnter ( Collision collision )
	{
		if ( collision.gameObject.name == "Player" )
		{
			var health = collision.gameObject.GetComponent<Health>();
			
			if ( health != null )
			{
				health.takeDamage( damage );
			}
		}
	}
}
