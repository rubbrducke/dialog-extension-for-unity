﻿using UnityEngine;
using System.Collections;

public class ThrowObjectToLocation : MonoBehaviour {
	
	public float throwPower = 1000;
	public GameObject obj;
	public GameObject spotlight;
	public float throwAngle = 0;
	public Transform[] targets;
	public float delayThrow = 3;
	public bool repeat = true;
	
	void Start () {
		StartCoroutine (DelayThrow());
	}

	IEnumerator DelayThrow() {
		int iteration = 0;
		while (repeat || (iteration < targets.Length )) {
			ThrowObj(targets[iteration % targets.Length]);
			yield return new WaitForSeconds(delayThrow);
			iteration++;
		}

	}

	void ThrowObj(Transform location) {
		//Creates the throw GameObject
		Vector3 throwPosition = new Vector3(
			transform.position.x,
			transform.GetComponent<Collider>().bounds.max.y,
			transform.position.z
			);
		throwPosition += transform.forward*3f;

		Debug.DrawLine(throwPosition, throwPosition+=transform.forward*5);
		//Create the throwing object
		GameObject newObj = Instantiate(obj, 
	        throwPosition,
	        transform.rotation
        ) as GameObject;  

		//Creates a spotlight showing wear object will land
		//GameObject targetLight = Instantiate(spotlight, 
	        //location.position + Vector3.up*2,
	        //Quaternion.AngleAxis(90, Vector3.right)
        //) as GameObject; 

		//Set the spotlight to be destroyed when throwing object is destroyed
		//newObj.GetComponent<DestroyOnImpact>().destroyAlso = new GameObject[1];
		//newObj.GetComponent<DestroyOnImpact>().destroyAlso[0] = targetLight;

		//Add the force to the throw object
		Vector3 dir = CalculateForce (location, throwAngle);
		Debug.DrawLine (transform.position, location.position);
		newObj.GetComponent<Rigidbody>().velocity = dir;
	}

	Vector3 CalculateForce(Transform target, float throwAngle) {
		//Get target direction
		Vector3 dir = target.position - transform.position;
		//Get Height Difference
		float h = dir.y;
		//Retain only horizontal Dir
		dir.y = 0;
		//Get horizontal Distance
		float dist = dir.magnitude;
		//Convert angle to radians
		throwAngle = throwAngle * Mathf.Deg2Rad;
		//Set Dir to elevation angle
		dir.y = dist * Mathf.Tan (throwAngle);
		//Correct for height difference
		dist += h / Mathf.Tan (throwAngle);
		//Calculate Velocity Magnitude
		float velocity = Mathf.Sqrt (dist * Physics.gravity.magnitude / Mathf.Sin (2 * throwAngle));

		return velocity * dir.normalized;
		
	}
}
